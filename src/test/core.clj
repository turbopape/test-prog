(ns test.core
  (:require [clojure.core.async :refer [chan >! >!! <!! go thread close!]]
            [clojure.core.reducers :as r])
  (:import [java.io BufferedReader Reader]))

(defn get-files-lines
  "This function, thanks to line-seq, generates a lazy sequence
  of the file name"
  [file-name]
  (line-seq (BufferedReader. (clojure.java.io/reader file-name)))) ; this is a lazy seq. We don't hold it in RAM, and
                                        ; only access it when we traverse the seq

(defn insert-into-topN
  
  [n cap-seq elem]
  {:pre [(set? cap-seq)]}
  (cond
    (some #{elem} cap-seq) cap-seq ; If the element is already in the topN set, skip
    (< (count cap-seq) n) (conj cap-seq elem) ; if there's room, add it
    (some #(> elem %) cap-seq ) (let [to-remove (last cap-seq)] ; add an element only if it's
                                        ; bigger than any of the topN's set elements
                                        ; then remove the last, assuming it's a desc sorted set
               
                                  (-> cap-seq
                                      (conj elem)
                                      (disj to-remove)))
    :default cap-seq)) ;; else I leave the result as is



(defn topN
  "file-name: the input file
  n: top N elements"
  [file-name n]
  {:pre [(> n 0)]}
  (let [to-main-chan (chan) ;; The conveyor belt - the channel
        lines (get-files-lines file-name)]
    (thread ;; The reader go-routine 
      (doseq [line lines ] ;; for each line
        (try (>!!  to-main-chan (Integer/parseInt (clojure.string/trim line)))
             (catch clojure.lang.ExceptionInfo e 0)
             )) ; throw this element inside the conveyor belt
      (close! to-main-chan)) ;; when we're done, close the channel to notify that the reading is done 
    
    (loop [elem (<!! to-main-chan) ; now for the topN keeping main thread
                                        ; while There's something coming from the reader
                                        ; i.e the channel is not closed
           result (sorted-set-by > )]
      (if elem
        (recur (<!! to-main-chan)
               (insert-into-topN n result elem)) ; use the function to keep the top N elements as they flow
        result))))








