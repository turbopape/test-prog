# test

Some test program

## Usage

Make sure you have JAVA 8 installed.
Install [leiningen](https://leiningen.org) to be able to run Clojure
projects.
Clone this repository, cd into it than issue:

```shell
lein repl
```
You'll then have a repl. launch the topN function no a file to get the
top N greatest numbers in your file:

```shell
test.core> (topN "data2.txt" 4)
#{2000 1000 399 100}
```

## License

Copyright © 2017 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
